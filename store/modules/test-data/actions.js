import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {
    [Constants.DO_LIST]: (store) => {
        // 백이 페이징되어있을 때 : 예. /all/{pageNum}
        // return axios.get(apiUrls.DO_LIST.replace('{pageNum}', payload.pageNum))
        return axios.get(apiUrls.DO_LIST)
    },
    [Constants.DO_DETAIL]: (store, payload) => {
        return axios.get(apiUrls.DO_DETAIL.replace('{id}', payload.id))
    },
    [Constants.DO_UPDATE]: (store, payload) => {
        return axios.get(apiUrls.DO_UPDATE.replace('{id}', payload.id))
    },
    [Constants.DO_DELETE]: (store, payload) => {
        return axios.get(apiUrls.DO_DELETE.replace('{id}', payload.id))
    },

}
