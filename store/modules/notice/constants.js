export default {
    DO_NOTICE_LIST: 'notice/doNoticeList',
    FETCH_NOTICE_LIST: 'notice/fetchNoticeList',
    GET_NOTICE_LIST: 'notice/getNoticeList',

    FETCH_TOTAL_ITEM_COUNT: 'notice/fetchTotalItemCount',
    FETCH_TOTAL_PAGE: 'notice/fetchTotalPage',
    FETCH_CURRENT_PAGE: 'notice/fetchCurrentPage',

    GET_TOTAL_ITEM_COUNT: 'notice/getTotalItemCount',
    GET_TOTAL_PAGE: 'notice/getTotalPage',
    GET_CURRENT_PAGE: 'notice/getCurrentPage',
}
