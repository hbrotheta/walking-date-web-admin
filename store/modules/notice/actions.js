import axios from 'axios'
import apiUrls from './api-urls'
import Constants from './constants'

export default {

    [Constants.DO_NOTICE_LIST]: (store, payload) => {
        let paramArray = []
        if (payload.params.monthValue != null) paramArray.push(`dateMonth=${payload.params.monthValue}`)
        if (payload.params.yearValue != null) paramArray.push(`dateYear=${payload.params.yearValue}`)
        if (payload.params.searchTitle.length >= 1) paramArray.push(`searchTitle=${payload.params.searchTitle}`)
        let paramText = paramArray.join('&')

        return axios.get(apiUrls.DO_NOTICE_LIST.replace('{pageNum}', payload.pageNum) + '?' + paramText)
    },

    // [Constants.DO_NOTICE_LIST]: (store, payload) => {
    //     return axios.get(apiUrls.DO_NOTICE_LIST.replace('{pageNum}', payload.pageNum) +'?dateMonth=' + payload.params.monthValue + '&dateYear=' + payload.params.yearValue + '&searchTitle=' + payload.params.searchTitle)
    // },
}
