const BASE_URL = '/v1/member'

export default {
    DO_MEMBER_LIST: `${BASE_URL}/information/{pageNum}`,
}
