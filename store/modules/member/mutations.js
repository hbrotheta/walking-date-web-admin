import Constants from './constants'
import state from './state'

export default {
    [Constants.FETCH_MEMBER_LIST]: (state, payload) => {
        state.list = payload
    },
    [Constants.FETCH_TOTAL_ITEM_COUNT]: (state, payload) => {
        state.totalItemCount = payload
    },
    [Constants.FETCH_TOTAL_PAGE]: (state, payload) => {
        state.totalPage = payload
    },
    [Constants.FETCH_CURRENT_PAGE]: (state, payload) => {
        state.currentPage = payload
    },
}
