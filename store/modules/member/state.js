const state = () => ({
    list: [],
    totalItemCount: 0,
    totalPage: 0,
    currentPage: 1,
})

export default state
