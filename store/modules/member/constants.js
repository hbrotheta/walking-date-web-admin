export default {
    DO_MEMBER_LIST: 'member/doMemberList',
    FETCH_MEMBER_LIST: 'member/fetchMemberList',
    GET_MEMBER_LIST: 'member/getMemberList',

    FETCH_TOTAL_ITEM_COUNT: 'member/fetchTotalItemCount',
    FETCH_TOTAL_PAGE: 'member/fetchTotalPage',
    FETCH_CURRENT_PAGE: 'member/fetchCurrentPage',

    GET_TOTAL_ITEM_COUNT: 'member/getTotalItemCount',
    GET_TOTAL_PAGE: 'member/getTotalPage',
    GET_CURRENT_PAGE: 'member/getCurrentPage',
}
